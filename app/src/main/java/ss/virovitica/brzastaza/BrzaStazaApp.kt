package ss.virovitica.brzastaza

import androidx.multidex.MultiDexApplication
import io.realm.Realm
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import ss.virovitica.brzastaza.di.appComponent

class BrzaStazaApp: MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@BrzaStazaApp)
            modules(appComponent)
        }
        Realm.init(this)
    }
}