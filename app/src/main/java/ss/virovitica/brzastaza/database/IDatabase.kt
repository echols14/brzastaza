package ss.virovitica.brzastaza.database

import ss.virovitica.brzastaza.model.CountyRadarData

interface IDatabase {
    fun clearDatabase()
    fun saveToDatabase(dailyRadars: CountyRadarData)
    fun getAllDailyRadars(): List<CountyRadarData>
    fun getDailyRadar(countyID: Int): CountyRadarData?
}