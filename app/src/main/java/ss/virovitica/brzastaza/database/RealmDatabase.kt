package ss.virovitica.brzastaza.database

import ss.virovitica.brzastaza.model.CountyRadarData
import ss.virovitica.brzastaza.utility.clearRealm
import ss.virovitica.brzastaza.utility.getAllDailyRadarsFromRealm
import ss.virovitica.brzastaza.utility.getDailyRadarFromRealm
import ss.virovitica.brzastaza.utility.saveToRealm

class RealmDatabase: IDatabase {
    override fun clearDatabase() = clearRealm()
    override fun saveToDatabase(dailyRadars: CountyRadarData) = saveToRealm(dailyRadars)
    override fun getAllDailyRadars(): List<CountyRadarData> = getAllDailyRadarsFromRealm()
    override fun getDailyRadar(countyID: Int) = getDailyRadarFromRealm(countyID)
}