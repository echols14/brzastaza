package ss.virovitica.brzastaza.utility

import android.content.Context
import android.view.View

val View.ctx: Context
    get() = context