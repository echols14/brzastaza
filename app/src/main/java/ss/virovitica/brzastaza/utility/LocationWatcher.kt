package ss.virovitica.brzastaza.utility

import android.util.Log
import com.google.android.gms.location.*

class LocationWatcher(private val fusedLocationClient: FusedLocationProviderClient, private val manager: LocationManager) {
    //members
    private val listener = object: LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            locationResult ?: return
            val location = locationResult.lastLocation
            manager.handleLocationUpdate(location)
            super.onLocationResult(locationResult)
        }
    }

    //functions

    fun startWatching() {
        val locationRequest = LocationRequest.create()?.apply {
            interval = 10*1000 //milliseconds
            fastestInterval = 5*1000 //milliseconds
            priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        }
        try {
            fusedLocationClient.requestLocationUpdates(locationRequest, listener, null)
        }
        catch(e: SecurityException){
            Log.e("LocationWatcher", "Location permissions not enabled")
        }

    }

    fun stopWatching() {
        fusedLocationClient.removeLocationUpdates(listener)
    }
}