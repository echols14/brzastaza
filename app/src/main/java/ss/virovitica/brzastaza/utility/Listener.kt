package ss.virovitica.brzastaza.utility

import ss.virovitica.brzastaza.model.CountyRadarData

interface RadarListener {
    fun onSuccess(result: CountyRadarData)
    fun onRadarError()
}

interface StringListener {
    fun onSuccess(result: String)
    fun onStringError()
}