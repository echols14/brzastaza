package ss.virovitica.brzastaza.utility

import android.content.Context
import com.google.gson.Gson
import ss.virovitica.brzastaza.R
import ss.virovitica.brzastaza.model.FixedRadar
import ss.virovitica.brzastaza.model.MarkerBox
import java.io.BufferedReader
import java.io.InputStreamReader

class FixedRadarReader {
    companion object {
        private var radars: List<FixedRadar>? = null
        fun getFixedRadars(context: Context): List<FixedRadar> {
            //if we've already retrieved the radars from the XML file, just return them
            if(radars != null) return radars!!
            /*//parse the XML file into Marker objects
            val mapper = XmlMapper()
            val xml = context.resources.openRawResource(R.raw.fixed_radars)
            val box = mapper.readValue(xml, MarkerBox::class.java)*/
            val bufferedReader = BufferedReader(InputStreamReader(context.resources.openRawResource(R.raw.fixed_radars_json)))
            val gson = Gson()
            val box = gson.fromJson(bufferedReader, MarkerBox::class.java)
            radars = box.toFixedRadars()
            return radars!!
        }
    }
}