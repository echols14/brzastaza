package ss.virovitica.brzastaza.utility

import io.realm.Realm
import io.realm.RealmResults
import ss.virovitica.brzastaza.model.CountyRadarData
import ss.virovitica.brzastaza.model.CountyRadarData.Companion.FIELD_COUNTY_ID

fun clearRealm() {
    val realm = Realm.getDefaultInstance()
    realm.executeTransaction {
        it.where(CountyRadarData::class.java).findAll().deleteAllFromRealm()
    }
    realm.close()
}

fun saveToRealm(dailyRadars: CountyRadarData) {
    val realm = Realm.getDefaultInstance()
    realm.executeTransaction{ it.copyToRealm(dailyRadars) }
    realm.close()
}

fun getAllDailyRadarsFromRealm(): List<CountyRadarData> {
    val realm = Realm.getDefaultInstance()
    val result: RealmResults<CountyRadarData> = realm.where(CountyRadarData::class.java).findAll()
    val toReturn = realm.copyFromRealm(result)
    realm.close()
    return toReturn
}

fun getDailyRadarFromRealm(countyID: Int): CountyRadarData? {
    val realm = Realm.getDefaultInstance()
    val result = realm.where(CountyRadarData::class.java).equalTo(FIELD_COUNTY_ID, countyID).findFirst()
    var toReturn: CountyRadarData? = null
    if(result != null) toReturn = realm.copyFromRealm(result)
    realm.close()
    return toReturn
}