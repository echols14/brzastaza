package ss.virovitica.brzastaza.utility

import android.content.Context

interface IView {
    val ctx: Context
}