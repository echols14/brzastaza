package ss.virovitica.brzastaza.utility

import android.location.Location
import ss.virovitica.brzastaza.model.FixedRadar
import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt

fun getDistanceFromLatLonInKm(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
    val radius = 6371.0 // Radius of the earth in km
    val dLat = deg2rad(lat2-lat1)
    val dLon = deg2rad(lon2-lon1)
    val a =
        sin(dLat/2) * sin(dLat/2) +
                cos(deg2rad(lat1)) * cos(deg2rad(lat2)) *
                sin(dLon/2) * sin(dLon/2)
    val c = 2 * atan2(sqrt(a), sqrt(1-a))
    return radius * c // Distance in km
}

fun deg2rad(deg: Double) = deg*(Math.PI/180.0)

fun checkForNearby(myLocation: Location, radars: List<FixedRadar>, radius: Double): Boolean {
    for(radar in radars) {
        if(getDistanceFromLatLonInKm(myLocation.latitude, myLocation.longitude,
                radar.latitude, radar.longitude) <= radius) return true
    }
    return false
}

fun getNearestDistance(myLocation: Location, radars: List<FixedRadar>): Double {
    var smallestVal = Double.MAX_VALUE
    for(radar in radars) {
        val thisDistance = getDistanceFromLatLonInKm(myLocation.latitude, myLocation.longitude, radar.latitude, radar.longitude)
        if(thisDistance < smallestVal) {
            smallestVal = thisDistance
        }
    }
    return smallestVal
}

fun <T> moveToFirst(index: Int, list: List<T>): List<T> {
    if(index >= list.size) return list
    val toReturn = ArrayList<T>(list.size)
    toReturn.add(list[index])
    for((i, item) in list.withIndex()) {
        if(i == index) continue
        toReturn.add(item)
    }
    return toReturn
}

val townNameMap = mapOf(
    "�upanijska" to "Županijska",
    "ju�na" to "južna",
    "Nu�tar" to "Nuštar",
    "Ceri�" to "Cerić",
    "Vo�inci" to "Vođinci",
    "Andrija�evci" to "Andrijaševci",
    "Moko�ica" to "Mokošica",
    "Metkovi�" to "Metković",
    "Kre�imira" to "Krešimira",
    "Asi�koga" to "Asiškoga",
    "Horva�anska" to "Horvaćanska",
    "Rogovi�i" to "Rogovići",
    "U�ka" to "Učka",
    "Bu�inija" to "Bužinija",
    "�uba" to "Đuba",
    "Jur�i�i" to "Juršići",
    "�minj" to "Žminj",
    "Mikleu�" to "Mikleuš",
    "Radi�a" to "Radića",
    "Luka�" to "Lukač",
    "Ba�evac" to "Bačevac",
    "P�eli�" to "Pčelić",
    "�panat" to "Španat",
    "�krinjari" to "Škrinjari",
    "�emovci" to "Šemovci",
    "Oku�ani" to "Okučani",
    "Hra��an" to "Hrašćan",
    "Doma�inec" to "Domašinec",
    "Dr�i�eva" to "Držićeva",
    "Zagreba�ka" to "Zagrebačka",
    "Pitoma�a" to "Pitomača",
    "-Strani�i" to "-Stranići",
    "Fa�ana" to "Fažana",
    "�enkovec" to "Šenkovec",
    "�trigova" to "Štrigova",
    "�emernica" to "Čemernica",
    "Kri�evci" to "Križevci",
    "Koprivni�ki" to "Koprivnički",
    "�akovec" to "Čakovec",
    "Ju�na" to "Južna",
    "JU�NA" to "JUŽNA",
    "Kri�" to "Križ",
    "Karlova�ka" to "Karlovačka",
    "Vara�din" to "Varaždin",
    "Vine�" to "Vinež",
    "�upanja" to "Županja",
    "Sva�i�a" to "Svačića",
    "Lu�ani" to "Lužani",
    "Ore�ac" to "Orešac",
    "Kri�eva�ka" to "Križevačka",
    "Sredi��e" to "Središće",
    "Hlapi�ina" to "Hlapičina",
    "Frater��ica" to "Frateršćica",
    "�vor" to "čvor",
    "Kri�eva�ki" to "Križevački",
    "�ur�evac" to "Đurđevac",
    "Nedeli��e" to "Nedelišće",
    "Badli�an" to "Badličan",
    "Budan�evica" to "Budančevica",
    "�abno" to "Žabno",
    "Dra�kovec" to "Draškovec",
    "Brse�ine" to "Brsečine",
    "Ora�ac" to "Orašac",
    "Klo�tar" to "Kloštar",
    "�tefanec" to "Štefanec",
    "Medve��ak" to "Medveščak",
    "Vi�njan" to "Višnjan",
    "Po�etna" to "Početna",
    "Pantov�ak" to "Pantovčak",
    "Br�adin" to "Bršadin",
    "Tr��anska" to "Tršćanska",
    "Svetvin�enat" to "Svetvinčenat",
    "Otri�" to "Otrić",
    "�rnovo" to "Žrnovo",
    "Po�ane" to "Požane",
    "Bukova�ki" to "Bukovački",
    "Bu�etina" to "Bušetina",
    "Baki�" to "Bakić",
    "Preradovi�a" to "Preradovića",
    "Gu��erovec" to "Guščerovec",
    "Moko�ica-�ajkovi�i" to "Mokošica-Čajkovići",
    "Star�evi�a" to "Starčevića",
    "�eletovci" to "Đeletovci",
    "Repa�" to "Repaš",
    "Klai�eva" to "Klaićeva",
    "Ba�anija" to "Bašanija",
    "Bo�e" to "Bože",
    "Milanovi�" to "Milanović",
    "Okruglja�a" to "Okrugljača",
    "�pi�i�" to "Špišić",
    "Plav�inac" to "Plavšinac",
    "Kur�anec" to "Kuršanec",
    "�i�an" to "Šišan",
    "Peli�eti" to "Peličeti",
    "�ajkovi�i" to "Čajkovići"
)

fun correctDiacritics(input: String): String {
    //check if there was a problem, save some time if there wasn't
    if(!input.contains('�')) return input
    //create a mutable string to receive all corrections
    var runningVal = input
    for(baseForm in townNameMap.keys) {
        //find a correction we know how to handle
        if(!runningVal.contains(baseForm)) continue
        //perform the replacement
        val newForm = townNameMap[baseForm]
        if(newForm != null) runningVal = runningVal.replace(baseForm, newForm)
    }
    return runningVal
}

