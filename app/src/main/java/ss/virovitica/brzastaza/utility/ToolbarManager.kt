package ss.virovitica.brzastaza.utility

import androidx.appcompat.graphics.drawable.DrawerArrowDrawable
import androidx.appcompat.widget.Toolbar
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import ss.virovitica.brzastaza.R
import ss.virovitica.brzastaza.activity.help.HelpActivity
import ss.virovitica.brzastaza.activity.radar.view.RadarActivity

interface ToolbarManager {
    val toolbar: Toolbar

    var toolbarTitle: String
        get() = toolbar.title.toString()
        set(value){
            toolbar.title = value
        }

    fun initToolbarMain(){
        toolbar.inflateMenu(R.menu.menu_main)
        toolbar.setOnMenuItemClickListener{
            when(it.itemId){
                R.id.action_help -> openHelp()
                R.id.action_daily_radars -> toolbar.ctx.startActivity<RadarActivity>()
                else -> toolbar.ctx.toast("Unknown option")
            }
            true
        }
    }

    fun initToolbarSmall(){
        toolbar.inflateMenu(R.menu.menu_small)
        toolbar.setOnMenuItemClickListener{
            when(it.itemId){
                R.id.action_help -> openHelp()
                else -> toolbar.ctx.toast("Unknown option")
            }
            true
        }
    }

    private fun openHelp() { toolbar.ctx.startActivity<HelpActivity>() }

    fun enableHomeAsUp(up: () -> Unit) {
        toolbar.navigationIcon = createUpDrawable()
        toolbar.setNavigationOnClickListener { up() }
    }

    private fun createUpDrawable() = DrawerArrowDrawable(toolbar.ctx).apply { progress = 1f }
}