package ss.virovitica.brzastaza.utility

import android.content.Context
import android.location.Location

interface LocationManager {
    val context: Context
    fun handleLocationUpdate(location: Location)
}