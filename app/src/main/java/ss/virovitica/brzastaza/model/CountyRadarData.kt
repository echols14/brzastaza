package ss.virovitica.brzastaza.model

import com.google.android.gms.maps.model.LatLng
import io.realm.RealmList
import io.realm.RealmObject
import org.jsoup.nodes.Document
import org.jsoup.nodes.TextNode
import ss.virovitica.brzastaza.utility.correctDiacritics
import java.util.*

open class CountyRadarData(): RealmObject() {
    companion object {
        const val FIELD_COUNTY_ID = "countyID"
        const val NOT_FOUND = -1

        val INDEX_TO_COUNTY_NAME = arrayOf("Zagrebačka", "Krapinsko-Zagorska", "Sisačko-Moslavačka",
            "Karlovačka", "Varaždinska", "Koprivničko-Križevačka", "Bjelovarsko-Bilogorska", "Primosrko-Goranska",
            "Ličko-Senjska", "Virovitičko-Podravska", "Požeško-Slavonska", "Brodsko-Posavska", "Zadarska", "Osječko-Baranjska",
            "Šibensko-Kninska", "Vukovarsko-Srijemska", "Splitsko-Dalmatinska", "Istarska", "Dubrovačko-Neretvanska", "Međimurska")

        val INDEX_TO_LAT_LON = arrayOf(LatLng(45.785397, 16.074369), LatLng(46.101930, 15.939374), LatLng(45.376738, 16.437509),
            LatLng(45.304192, 15.500659), LatLng(46.253515, 16.322621), LatLng(46.138873, 16.852673), LatLng(45.730801, 16.999024), LatLng(45.350681, 14.689447),
            LatLng(44.681814, 15.414512), LatLng(45.750957, 17.595237), LatLng(45.416112, 17.575117), LatLng(45.183366, 17.822601), LatLng(44.150325, 15.668157), LatLng(45.562359, 18.491936),
            LatLng(43.879044, 16.074173), LatLng(45.178809, 18.882200), LatLng(43.506644, 16.785036), LatLng(45.201760, 13.888341), LatLng(42.828604, 17.821974), LatLng(46.401447, 16.540628))

        fun findCountyIndex(searchVal: String): Int {
            for((i, county) in INDEX_TO_COUNTY_NAME.withIndex()) {
                if(searchVal.toLowerCase() == county.toLowerCase()) return i
            }
            return NOT_FOUND
        }

        fun mapFromHtml(document: Document, countyIndex: Int): CountyRadarData {
            val tempList: RealmList<RadarNode> = RealmList()
            val dateNode = document.childNode(0).childNode(1).childNode(0).childNode(2).childNode(0)
            var date = ""
            if(dateNode is TextNode) {
                val dateString = dateNode.text()
                val toSkip = "Radari "
                date = dateString.substring(toSkip.length, dateString.length)
            }
            val locationNodes = document.childNode(0).childNode(1).childNode(2).childNode(0).childNodes()
            for(node in locationNodes){
                if(node is TextNode) continue
                var route = ""
                val routeTextNode = node.childNode(0).childNode(0)
                if(routeTextNode is TextNode) route = correctDiacritics(routeTextNode.text())
                var startTime = ""
                var endTime = ""
                val timeTextNode = node.childNode(1).childNode(0)
                if(timeTextNode is TextNode) {
                    val time = timeTextNode.text()
                    val scanner = Scanner(time).useDelimiter("-")
                    if(scanner.hasNext()) startTime = scanner.next()
                    if(scanner.hasNext()) endTime = scanner.next()
                }
                tempList.add(RadarNode(route, startTime, endTime))
            }
            return CountyRadarData(date, tempList, countyIndex)
        }
    }

    //members
    var date = ""
        private set
    var radarList = RealmList<RadarNode>()
        private set
    var countyID = -1
        private set

    //constructor
    constructor(dateIn: String, radarListIn: RealmList<RadarNode>, countyIndexIn: Int): this() {
        date = dateIn
        radarList = radarListIn
        countyID = countyIndexIn
    }
}

open class RadarNode(): RealmObject() {
    //members
    var route = ""
        private set
    var startTime = ""
        private set
    var endTime = ""
        private set

    //constructor
    constructor(routeIn: String, startTimeIn: String, endTimeIn: String): this() {
        route = routeIn
        startTime = startTimeIn
        endTime = endTimeIn
    }
}