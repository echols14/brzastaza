package ss.virovitica.brzastaza.model

class MarkerBox {
    var markers: List<Marker> = ArrayList()
        private set

    fun toFixedRadars(): List<FixedRadar> {
        val list = ArrayList<FixedRadar>(markers.size)
        markers.forEach {
            list.add(it.toFixedRadar())
        }
        return list
    }
}

class Marker {
    var id: String? = null
        private set
    var x: String? = null
        private set
    var y: String? = null
        private set
    var c: String? = null
        private set
    fun toFixedRadar(): FixedRadar {
        val newId = id?.toInt() ?: -1
        val latitude = y?.toDouble() ?: 0.0
        val longitude = x?.toDouble() ?: 0.0
        return FixedRadar(newId, latitude, longitude)
    }
}