package ss.virovitica.brzastaza.model

data class FixedRadar(val id: Int, val latitude: Double, val longitude: Double)