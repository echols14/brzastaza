package ss.virovitica.brzastaza.model

data class ReverseGeocodeResponse(val Response: Response)
data class Response(val View: Array<ViewBox>) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as Response
        if (!View.contentEquals(other.View)) return false
        return true
    }
    override fun hashCode() = View.contentHashCode()
}
data class ViewBox(val Result: Array<ResultBox>) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as ViewBox
        if (!Result.contentEquals(other.Result)) return false
        return true
    }
    override fun hashCode() = Result.contentHashCode()
}
data class ResultBox(val Location: Location)
data class Location(val Address: Address)
data class Address(val PostalCode: String?, val County: String)