package ss.virovitica.brzastaza.interactor.radar

import ss.virovitica.brzastaza.utility.RadarListener

interface IRadarInteractor {
    fun fetchDailyRadars(listener: RadarListener, countyIndex: Int)
    fun unsubscribe()
}