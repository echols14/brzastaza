package ss.virovitica.brzastaza.interactor.radar

import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import org.jsoup.Jsoup
import ss.virovitica.brzastaza.model.CountyRadarData
import ss.virovitica.brzastaza.net.HakClient
import ss.virovitica.brzastaza.utility.RadarListener

class RadarInteractor(private val hakClient: HakClient): IRadarInteractor {
    //members
    private var disposable = CompositeDisposable()

    //functions

    private fun pullDocument(response: ResponseBody) = Jsoup.parse(response.string())

    //***IRadarInteractor***//

    override fun fetchDailyRadars(listener: RadarListener, countyIndex: Int) {
        disposable.add(
            hakClient.getDailyRadars(countyIndex.toString())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(object : DisposableObserver<ResponseBody>() {
                    override fun onNext(response: ResponseBody) {
                        val doc = pullDocument(response)
                        val data = CountyRadarData.mapFromHtml(doc, countyIndex)
                        Log.i("RadarInteractor", "daily radars retrieved")
                        listener.onSuccess(data)
                    }
                    override fun onError(e: Throwable) {
                        Log.e("RadarInteractor", e.toString())
                        listener.onRadarError()
                    }
                    override fun onComplete() {}
                })
        )
    }

    override fun unsubscribe() {
        disposable.dispose()
    }
}