package ss.virovitica.brzastaza.interactor.geocoding

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import ss.virovitica.brzastaza.model.ReverseGeocodeResponse
import ss.virovitica.brzastaza.net.GeocodeClient
import ss.virovitica.brzastaza.net.GeocodeClient.Companion.APP_CODE_VAL
import ss.virovitica.brzastaza.net.GeocodeClient.Companion.APP_ID_VAL
import ss.virovitica.brzastaza.net.GeocodeClient.Companion.C
import ss.virovitica.brzastaza.net.GeocodeClient.Companion.GEN_VAL
import ss.virovitica.brzastaza.net.GeocodeClient.Companion.MAX_RESULTS_VAL
import ss.virovitica.brzastaza.net.GeocodeClient.Companion.MODE_VAL
import ss.virovitica.brzastaza.net.GeocodeClient.Companion.RADIUS
import ss.virovitica.brzastaza.utility.StringListener

class GeocodeInteractor(private val geocodeClient: GeocodeClient): IGeocodeInteractor {
    companion object{
        private const val TO_REMOVE = " županija"
        private fun extractCounty(response: ReverseGeocodeResponse): String? {
            val view = response.Response.View
            if(view.isEmpty()) return null
            val result = view[0].Result
            if(result.isEmpty()) return null
            var county = result[0].Location.Address.County
            county = county.replace(TO_REMOVE, "")
            return county
        }
    }
    private var disposable: CompositeDisposable? = null
    override fun reverseGeocode(latitude: Double, longitude: Double, listener: StringListener) {
        disposable = CompositeDisposable()
        val proxVal = latitude.toString() + C + longitude.toString() + C + RADIUS
        disposable?.add(geocodeClient.getAddress(proxVal, MODE_VAL, MAX_RESULTS_VAL, GEN_VAL, APP_ID_VAL, APP_CODE_VAL)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeWith(object: DisposableObserver<ReverseGeocodeResponse>(){
                override fun onNext(response: ReverseGeocodeResponse) {
                    val fullCountyName = extractCounty(response)
                    if(fullCountyName != null) listener.onSuccess(fullCountyName)
                    else listener.onStringError()
                }
                override fun onError(e: Throwable) = listener.onStringError()
                override fun onComplete() {}
            })
        )
    }

    override fun unsubscribe() {
        if(disposable?.isDisposed == false) disposable?.dispose()
    }
}