package ss.virovitica.brzastaza.interactor.geocoding

import ss.virovitica.brzastaza.utility.StringListener

interface IGeocodeInteractor {
    fun reverseGeocode(latitude: Double, longitude: Double, listener: StringListener)
    fun unsubscribe()
}