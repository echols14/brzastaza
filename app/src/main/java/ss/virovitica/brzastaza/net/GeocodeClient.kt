package ss.virovitica.brzastaza.net

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query
import ss.virovitica.brzastaza.model.ReverseGeocodeResponse

interface GeocodeClient {
    companion object{
        const val GEOCODE_API_OKHTTP = "GeocodeApiOkHttp"
        const val GEOCODE_API_RETROFIT = "GeocodeApiRetrofit"
        const val TIMEOUT_SECONDS: Long = 10
        const val BASE_URL = "https://reverse.geocoder.api.here.com/"
        private const val PATH = "6.2/reversegeocode.json"

        private const val PROX = "prox"
        private const val MODE = "mode"
        private const val MAX_RESULTS = "maxresults"
        private const val GEN = "gen"
        private const val APP_ID = "app_id"
        private const val APP_CODE = "app_code"

        const val C = ","
        const val RADIUS = "250"
        const val MODE_VAL = "retrieveAddresses"
        const val MAX_RESULTS_VAL = "1"
        const val GEN_VAL = "9"
        const val APP_ID_VAL = "o44vSe7QdmWFDrs06a02"
        const val APP_CODE_VAL = "1YE95C5tqQlZTOHG8gW3WA"
    }

    @GET(PATH)
    fun getAddress(@Query(PROX) prox: String, @Query(MODE) mode: String, @Query(
        MAX_RESULTS
    ) max: String,
                   @Query(GEN) gen: String, @Query(APP_ID) id: String, @Query(
            APP_CODE
        ) code: String): Observable<ReverseGeocodeResponse>
}