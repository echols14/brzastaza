package ss.virovitica.brzastaza.net

import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Query

interface HakClient {
    companion object {
        const val HAK_API_OKHTTP = "HakApiOkHttp"
        const val HAK_API_RETROFIT = "HakApiRetrofit"
        const val TIMEOUT_SECONDS: Long = 10
        const val BASE_URL = "https://m.hak.hr/"
        private const val PATH = "radari.asp"
        private const val Z = "z"
    }

    @GET(PATH)
    fun getDailyRadars(@Query(Z) z: String): Observable<ResponseBody>
}