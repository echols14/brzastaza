package ss.virovitica.brzastaza.fragment

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import ss.virovitica.brzastaza.R

class PermissionExplanationDialog: DialogFragment() {
    companion object {
        const val PERMISSION_EXPLANATION_DIALOG = "ss.virovitica.brzastaza.fragment.PermissionExplanationDialog"
    }

    interface DialogListener {
        fun onDismiss()
    }

    //members
    private lateinit var listener: DialogListener

    //functions

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setTitle(R.string.my_location)
            builder.setMessage(getString(R.string.location_request_explanation))
            builder.setPositiveButton(R.string.ok) { dialog, _ -> dialog.dismiss() }
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        // Verify that the host activity implements the callback interface
        try { //Instantiate the DialogListener so we can send events to the host
            listener = context as DialogListener
        } catch (e: ClassCastException) {
            throw ClassCastException(("$context must implement DialogListener"))
        }
    }

    override fun onDismiss(dialog: DialogInterface?) {
        listener.onDismiss()
        super.onDismiss(dialog)
    }
}