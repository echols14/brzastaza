package ss.virovitica.brzastaza.fragment

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.dialog_directions.view.*
import ss.virovitica.brzastaza.R

class DirectionsDialog: DialogFragment() {
    companion object {
        const val DIRECTIONS_DIALOG = "ss.virovitica.brzastaza.fragment.DirectionsDialog"
    }

    //functions

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            // Get the layout inflater
            val inflater = requireActivity().layoutInflater
            val dialogView = inflater.inflate(R.layout.dialog_directions, null)
            builder.setView(dialogView)
            builder.setTitle(R.string.start_navigation)
            builder.setPositiveButton(R.string.ok) { dialog, _ ->
                val destination = dialogView.destinationInput.editableText.toString()
                val gmmIntentUri = Uri.parse("google.navigation:mode=d&q=" + Uri.encode(destination))
                val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                mapIntent.setPackage("com.google.android.apps.maps")
                startActivity(mapIntent)
                dialog.dismiss()
            }
            builder.setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}