package ss.virovitica.brzastaza.activity.map.presenter

import android.location.Location

interface IMapPresenter {
    fun fetchRadars()
    fun unsubscribe()
    fun checkForNearbyRadars(location: Location)
    fun fetchNearestRadarDistance(location: Location)
    fun checkForNewCounty(location: Location)
}