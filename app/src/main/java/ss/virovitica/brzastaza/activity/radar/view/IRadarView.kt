package ss.virovitica.brzastaza.activity.radar.view

import ss.virovitica.brzastaza.model.CountyRadarData
import ss.virovitica.brzastaza.utility.IView

interface IRadarView: IView {
    fun setDailyRadars(dailyRadars: List<CountyRadarData>)
    fun errorMessage()
}