package ss.virovitica.brzastaza.activity.radar.presenter

import android.location.Location

interface IRadarPresenter {
    fun fetchRadars()
    fun sortRadars()
    fun sortRadars(location: Location)
}