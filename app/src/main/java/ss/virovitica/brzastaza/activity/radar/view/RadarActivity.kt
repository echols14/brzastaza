package ss.virovitica.brzastaza.activity.radar.view

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.android.gms.location.FusedLocationProviderClient
import kotlinx.android.synthetic.main.activity_daily_radars.*
import org.jetbrains.anko.find
import org.jetbrains.anko.longToast
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import ss.virovitica.brzastaza.R
import ss.virovitica.brzastaza.activity.radar.presenter.IRadarPresenter
import ss.virovitica.brzastaza.adapter.RadarExpandableAdapter
import ss.virovitica.brzastaza.model.CountyRadarData
import ss.virovitica.brzastaza.utility.ToolbarManager

class RadarActivity: AppCompatActivity(), IRadarView, ToolbarManager {
    //members
    override val ctx: Context
        get() = this
    override val toolbar: Toolbar by lazy { find<Toolbar>(R.id.toolbar) }
    private val presenter: IRadarPresenter by inject { parametersOf(this) }
    private val adapter = RadarExpandableAdapter()
    private val fusedLocationClient: FusedLocationProviderClient by inject { parametersOf(this) }
    private var progressBar: ProgressBar? = null

    //functions

    //***AppCompatActivity***//

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_daily_radars)
        //set up the toolbar
        initToolbarSmall()
        enableHomeAsUp{ onBackPressed() }
        toolbarTitle = getString(R.string.daily_radars)
        //set up the expandable list
        dailyRadarExpandable.setAdapter(adapter)
        //grab radars from database
        presenter.fetchRadars()
    }

    override fun onResume() {
        super.onResume()
        //ask for their location if we're allowed
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            //show progress bar
            showLoadingDialog()
            //get their location
            fusedLocationClient.lastLocation.addOnSuccessListener {
                if(it == null) presenter.sortRadars()
                else presenter.sortRadars(it)
            }
        }
        else {
            presenter.sortRadars()
        }
    }

    //***IRadarView***//

    override fun setDailyRadars(dailyRadars: List<CountyRadarData>) {
        removeLoadingDialog()
        if(dailyRadars.isNotEmpty()) dateText.text = dailyRadars[0].date
        else dateText.text = getString(R.string.daily_radars_failed)
        adapter.setRadars(dailyRadars)
    }

    override fun errorMessage() {
        longToast("No radars available")
    }

    //private

    private fun showLoadingDialog() {
        progressBar = ProgressBar(this, null, android.R.attr.progressBarStyleLarge)
        loadingDialogHolder.addView(progressBar)
        progressBar?.visibility = View.VISIBLE
        dateText.visibility = View.GONE
        dailyRadarExpandable.visibility = View.GONE
    }

    private fun removeLoadingDialog() {
        progressBar?.visibility = View.GONE
        loadingDialogHolder.visibility = View.GONE
        loadingDialogHolder.removeAllViews()
        dateText.visibility = View.VISIBLE
        dailyRadarExpandable.visibility = View.VISIBLE
        progressBar = null
    }
}