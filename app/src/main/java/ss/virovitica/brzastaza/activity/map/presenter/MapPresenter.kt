package ss.virovitica.brzastaza.activity.map.presenter

import android.location.Location
import android.util.Log
import org.jetbrains.anko.toast
import ss.virovitica.brzastaza.R
import ss.virovitica.brzastaza.activity.map.view.IMapView
import ss.virovitica.brzastaza.database.IDatabase
import ss.virovitica.brzastaza.interactor.geocoding.IGeocodeInteractor
import ss.virovitica.brzastaza.interactor.radar.IRadarInteractor
import ss.virovitica.brzastaza.model.FixedRadar
import ss.virovitica.brzastaza.model.CountyRadarData
import ss.virovitica.brzastaza.utility.*

class MapPresenter(private val view: IMapView, private val radarInteractor: IRadarInteractor,
                   private val geocodeInteractor: IGeocodeInteractor, private val database: IDatabase):
    IMapPresenter, RadarListener, StringListener {
    companion object {
        private const val DANGER_RADIUS_KM: Double = 2.0
        private const val NOT_FOUND = 0
    }
    //members
    private var fixedRadars: List<FixedRadar> = ArrayList()
    private var currentCountyID = NOT_FOUND

    //functions

    //***IMapPresenter***//

    override fun fetchRadars() {
        database.clearDatabase()
        for(i in 1..CountyRadarData.INDEX_TO_COUNTY_NAME.size) {
            radarInteractor.fetchDailyRadars(this, i)
        }
        fixedRadars = FixedRadarReader.getFixedRadars(view.ctx)
        view.drawFixedRadars(fixedRadars)
    }

    override fun checkForNearbyRadars(location: Location) {
        if(checkForNearby(location, fixedRadars, DANGER_RADIUS_KM)) view.sendFixedRadarAlert()
        else view.removeFixedRadarAlert()
    }

    override fun fetchNearestRadarDistance(location: Location) {
        val nearestRadarDistance = getNearestDistance(location, fixedRadars)
        if(nearestRadarDistance != Double.MAX_VALUE) view.setNearestRadarDistance(nearestRadarDistance)
    }

    override fun checkForNewCounty(location: Location) {
        geocodeInteractor.reverseGeocode(location.latitude, location.longitude, this)
    }

    override fun unsubscribe() { radarInteractor.unsubscribe() }

    //***RadarListener***//

    override fun onSuccess(result: CountyRadarData) {
        Log.i("MapPresenter", "Daily radars for ${result.date} saved to database")
        database.saveToDatabase(result)
    }

    override fun onRadarError() {
        Log.e("MapPresenter", "Daily radars could not be retrieved; radarInteractor sent an error")
        view.ctx.toast(view.ctx.getString(R.string.daily_radars_failed))
    }

    //***StringListener***//

    override fun onSuccess(result: String) {
        Log.i("MapPresenter", "Currently in county: $result")
        //match the county name to get its ID
        val countyID = CountyRadarData.findCountyIndex(result) + 1
        if(countyID == NOT_FOUND) { onStringError(); return } //county not found by name
        if(countyID == currentCountyID) return
        val countyRadars = database.getDailyRadar(countyID)
        if(countyRadars == null || countyRadars.radarList.size == 0) { view.removeDynamicRadarAlert() }
        else if(countyRadars.radarList.size != 0) { view.sendDynamicRadarAlert() }
        currentCountyID = countyID
    }

    override fun onStringError() {
        Log.e("MapPresenter", "Current county could not be retrieved, or did not match a Croatian county")
    }
}