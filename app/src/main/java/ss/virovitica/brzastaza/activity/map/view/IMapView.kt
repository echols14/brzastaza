package ss.virovitica.brzastaza.activity.map.view

import ss.virovitica.brzastaza.model.FixedRadar
import ss.virovitica.brzastaza.utility.IView

interface IMapView: IView {
    fun drawFixedRadars(fixedRadars: List<FixedRadar>)
    fun sendFixedRadarAlert()
    fun removeFixedRadarAlert()
    fun sendDynamicRadarAlert()
    fun removeDynamicRadarAlert()
    fun setNearestRadarDistance(nearestRadarDistance: Double)
}