package ss.virovitica.brzastaza.activity.help

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import kotlinx.android.synthetic.main.activity_help.*
import org.jetbrains.anko.find
import ss.virovitica.brzastaza.R
import ss.virovitica.brzastaza.utility.IView
import ss.virovitica.brzastaza.utility.ToolbarManager

class HelpActivity: AppCompatActivity(), ToolbarManager, IView {
    //members
    override val ctx: Context
        get() = this
    override val toolbar: Toolbar by lazy { find<Toolbar>(R.id.toolbar) }

    //functions

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help)
        //set up the toolbar
        enableHomeAsUp{ onBackPressed() }
        toolbarTitle = getString(R.string.info)
        //fill in the text view that needs help
        useBodyTextView.text = getString(R.string.use_body, getString(R.string.directions))
    }
}