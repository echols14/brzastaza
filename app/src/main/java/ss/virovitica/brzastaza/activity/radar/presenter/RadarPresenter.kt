package ss.virovitica.brzastaza.activity.radar.presenter

import android.location.Location
import ss.virovitica.brzastaza.activity.radar.view.IRadarView
import ss.virovitica.brzastaza.database.IDatabase
import ss.virovitica.brzastaza.interactor.geocoding.IGeocodeInteractor
import ss.virovitica.brzastaza.model.CountyRadarData
import ss.virovitica.brzastaza.model.CountyRadarData.Companion.NOT_FOUND
import ss.virovitica.brzastaza.utility.StringListener
import ss.virovitica.brzastaza.utility.getDistanceFromLatLonInKm
import ss.virovitica.brzastaza.utility.moveToFirst

class RadarPresenter(private val view: IRadarView, private val interactor: IGeocodeInteractor, private val database: IDatabase):
    IRadarPresenter, StringListener {
    //members
    private var radars: List<CountyRadarData> = ArrayList()

    //functions

    //***IRadarPresenter***//

    override fun fetchRadars() {
        radars = database.getAllDailyRadars()
        if(radars.isEmpty()) view.errorMessage()
    }

    override fun sortRadars() { //default sort: uses number of radars in the county
        radars = radars.sortedWith(compareBy {it.radarList.size}).reversed()
        view.setDailyRadars(radars)
    }

    override fun sortRadars(location: Location) { //sort by distance from the user
        //get the list of county indexes ordered by distance from the user's location
        val orderedIdList = getIdListByDistance(location)
        val orderedRadarList = ArrayList<CountyRadarData>(radars.size)
        //find each county by its index and save the new order
        for(id in orderedIdList) {
            for(county in radars) {
                if(id == county.countyID) {
                    orderedRadarList.add(county)
                    continue
                }
            }
        }
        radars = orderedRadarList
        //try to get the user's current county to put it first in the list
        interactor.reverseGeocode(location.latitude, location.longitude, this)
    }

    //***Listener***//

    override fun onSuccess(result: String) {
        //match the county name to get its ID
        val countyID = CountyRadarData.findCountyIndex(result) + 1
        if(countyID == NOT_FOUND + 1) { onStringError(); return } //county not found by name
        //find where the county with that name is in our saved list
        var index = NOT_FOUND
        for((i, county) in radars.withIndex()){
            if(county.countyID == countyID) { index = i; break }
        }
        if(index == NOT_FOUND) { onStringError(); return } //couldn't find the given county ID
        //move the particular county to the top of the list
        radars = moveToFirst(index, radars)
        view.setDailyRadars(radars)
    }

    override fun onStringError() { //cannot find current county, so keep the current sort
        view.setDailyRadars(radars)
    }

    //private

    private fun getIdListByDistance(myLocation: Location): List<Int> {
        //get each distance paired with its county's ID
        val distances = ArrayList<Pair<Int, Double>>() //ID to distance
        for((index, countyCenter) in CountyRadarData.INDEX_TO_LAT_LON.withIndex()) {
            val distance = getDistanceFromLatLonInKm(myLocation.latitude, myLocation.longitude, countyCenter.latitude, countyCenter.longitude)
            distances.add(Pair(index + 1, distance))
        }
        //sort the pairs by distance
        val sortedDistances = distances.sortedWith(compareBy{it.second})
        //copy the IDs into their own list
        val sortedIdList = ArrayList<Int>(sortedDistances.size)
        for(pair in sortedDistances) {
            sortedIdList.add(pair.first)
        }
        return sortedIdList
    }
}