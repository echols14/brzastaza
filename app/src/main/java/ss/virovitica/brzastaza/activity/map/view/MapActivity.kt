package ss.virovitica.brzastaza.activity.map.view

import android.Manifest
import android.app.NotificationChannel
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import com.google.android.gms.maps.*
import kotlinx.android.synthetic.main.activity_maps.*
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.maps.model.*
import org.jetbrains.anko.find
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import ss.virovitica.brzastaza.R
import ss.virovitica.brzastaza.activity.map.presenter.IMapPresenter
import ss.virovitica.brzastaza.fragment.DirectionsDialog
import ss.virovitica.brzastaza.fragment.DirectionsDialog.Companion.DIRECTIONS_DIALOG
import ss.virovitica.brzastaza.fragment.PermissionExplanationDialog
import ss.virovitica.brzastaza.fragment.PermissionExplanationDialog.Companion.PERMISSION_EXPLANATION_DIALOG
import ss.virovitica.brzastaza.model.FixedRadar
import ss.virovitica.brzastaza.utility.LocationWatcher
import ss.virovitica.brzastaza.utility.ToolbarManager
import kotlin.collections.ArrayList
import android.app.NotificationManager
import android.graphics.Color
import android.net.Uri
import android.os.Build
import ss.virovitica.brzastaza.utility.LocationManager
import android.app.Notification
import android.media.AudioAttributes
import android.view.View.VISIBLE
import org.jetbrains.anko.toast
import java.text.DecimalFormat
import java.util.*


class MapActivity: AppCompatActivity(), OnMapReadyCallback, IMapView, PermissionExplanationDialog.DialogListener,
    ToolbarManager, LocationManager {
    companion object {
        private const val INITIAL_ZOOM_VAL = 10F
        private const val REQUEST_LOCATION_PERMISSIONS = 42
        private const val FIXED_NOTIFICATION_ID = 0
        private const val DYNAMIC_NOTIFICATION_ID = 1
        private const val FIXED_ALERT_CHANNEL_ID = "343"
        private const val DYNAMIC_ALERT_CHANNEL_ID = "345"
    }
    //members
    override val ctx: Context
        get() = this
    override val context: Context
        get() = this
    override val toolbar: Toolbar by lazy { find<Toolbar>(R.id.toolbar) }
    private val presenter: IMapPresenter by inject { parametersOf(this) }
    private lateinit var map: GoogleMap
    private val fusedLocationClient: FusedLocationProviderClient by inject { parametersOf(this) }
    private var locationWatcher: LocationWatcher? = null
    private val fixedRadarMarkers: ArrayList<Marker> = ArrayList()

    //functions

    //***AppCompatActivity***//

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        //set up the toolbar
        initToolbarMain()
        toolbarTitle = getString(R.string.fixed_radars)
        //load the map
        val map: MapView = findViewById(R.id.map)
        map.onCreate(null)
        map.onResume()
        map.getMapAsync(this)
        //set up the button
        goButton.setOnClickListener {
            val dialog = DirectionsDialog()
            dialog.show(supportFragmentManager, DIRECTIONS_DIALOG)
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        askForLocation()
    }

    override fun onDestroy() {
        locationWatcher?.stopWatching()
        presenter.unsubscribe()
        super.onDestroy()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        enableMyLocation()
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    //***OnMapReadyCallback***//

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        askForLocation() //enable 'my location'
        presenter.fetchRadars() //load radars
    }

    //***IMapView***//

    override fun drawFixedRadars(fixedRadars: List<FixedRadar>) {
        //clear old markers
        for(marker in fixedRadarMarkers) { marker.remove() }
        fixedRadarMarkers.clear()
        fixedRadarMarkers.ensureCapacity(fixedRadars.size)
        //add new markers
        for(radar in fixedRadars) {
            val location = LatLng(radar.latitude, radar.longitude)
            //val bangIcon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_bang_extra_small)
            val bangBitmap = (resources.getDrawable(R.mipmap.ic_bang_red) as BitmapDrawable).bitmap
            val smallBangBitmap = Bitmap.createScaledBitmap(bangBitmap, 48, 48, false)
            val bangIcon = BitmapDescriptorFactory.fromBitmap(smallBangBitmap)
            val marker = map.addMarker(MarkerOptions().position(location).title(radar.id.toString()).icon(bangIcon))
            fixedRadarMarkers.add(marker)
        }
        //zoom in on user location if found, otherwise on an arbitrary marker
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fusedLocationClient.lastLocation.addOnSuccessListener {
                if(!zoomInOnUserLocation(it)) zoomInOnRadar(fixedRadars)
            }
        }
        else if(fixedRadars.isNotEmpty()) {
            zoomInOnRadar(fixedRadars)
        }
    }

    override fun sendFixedRadarAlert() {
        //create the intent that says what to do when the notification is clicked
        val intent = Intent(this, MapActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
        }
        val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
        //build the channel
        val builder: NotificationCompat.Builder
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val sound = Uri.parse("android.resource://ss.virovitica.brzastaza/raw/warning_short")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.fixed_alert_channel_name)
            val description = getString(R.string.fixed_alert_channel_description)
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(FIXED_ALERT_CHANNEL_ID, name, importance)
            val attributes = AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).build()
            channel.description = description
            channel.enableLights(true)
            channel.lightColor = Color.RED
            channel.setSound(sound, attributes)
            notificationManager.createNotificationChannel(channel)
            builder = NotificationCompat.Builder(this, FIXED_ALERT_CHANNEL_ID)
        } else {
            builder = NotificationCompat.Builder(this)
        }
        //build the notification
        builder.setSmallIcon(R.mipmap.ic_bang_red)
            .setContentTitle(getString(R.string.notification_title_fixed))
            .setContentText(getString(R.string.notification_text_fixed_radar))
            .setStyle(NotificationCompat.BigTextStyle()
                .bigText(getString(R.string.notification_big_text_fixed_radar)))
            //.setAutoCancel(true) //remove the notification when clicked
            .setContentIntent(pendingIntent) //attach the intent to use when clicked
            .setOnlyAlertOnce(true) //don't double notifications
            .setDefaults(Notification.DEFAULT_SOUND or Notification.DEFAULT_VIBRATE)
            .setSound(sound)
        builder.priority = NotificationCompat.PRIORITY_HIGH
        //send the notification
        notificationManager.notify(FIXED_NOTIFICATION_ID, builder.build())
    }

    override fun removeFixedRadarAlert() {
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(FIXED_NOTIFICATION_ID)
    }

    override fun sendDynamicRadarAlert() {
        //create the intent that says what to do when the notification is clicked
        val intent = Intent(this, MapActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
        }
        val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
        //build the channel
        val builder: NotificationCompat.Builder
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.dynamic_alert_channel_name)
            val description = getString(R.string.dynamic_alert_channel_description)
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(DYNAMIC_ALERT_CHANNEL_ID, name, importance)
            channel.description = description
            channel.enableLights(true)
            channel.lightColor = Color.BLUE
            notificationManager.createNotificationChannel(channel)
            builder = NotificationCompat.Builder(this, DYNAMIC_ALERT_CHANNEL_ID)
        } else {
            builder = NotificationCompat.Builder(this)
        }
        //build the notification
        builder.setSmallIcon(R.mipmap.ic_bang_blue)
            .setContentTitle(getString(R.string.notification_title_dynamic))
            .setContentText(getString(R.string.notification_text_dynamic_radar))
            .setStyle(NotificationCompat.BigTextStyle()
                .bigText(getString(R.string.notification_big_text_dynamic_radar)))
            .setAutoCancel(true) //remove the notification when clicked
            .setContentIntent(pendingIntent) //attach the intent to use when clicked
            .setOnlyAlertOnce(true) //don't double notifications
        builder.priority = NotificationCompat.PRIORITY_HIGH
        //send the notification
        notificationManager.notify(DYNAMIC_NOTIFICATION_ID, builder.build())
    }

    override fun removeDynamicRadarAlert() {
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(DYNAMIC_NOTIFICATION_ID)
    }

    override fun setNearestRadarDistance(nearestRadarDistance: Double) {
        val df = DecimalFormat.getInstance(Locale.getDefault()) as DecimalFormat
        df.applyPattern("#.#")
        nearestRadarText.text = getString(R.string.current_distance_from_nearest, df.format(nearestRadarDistance))
        radarDistanceView.visibility = VISIBLE
    }

    //***DialogListener***//

    override fun onDismiss() = ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_LOCATION_PERMISSIONS)

    //***LocationManager***//

    override fun handleLocationUpdate(location: Location) {
        presenter.checkForNearbyRadars(location)
        presenter.checkForNewCounty(location)
        presenter.fetchNearestRadarDistance(location)
    }

    //private

    private fun zoomInOnRadar(radars: List<FixedRadar>) {
        val location = LatLng(radars[0].latitude, radars[0].longitude)
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, INITIAL_ZOOM_VAL))
    }

    private fun zoomInOnUserLocation(location: Location?): Boolean {
        return if (location != null) {
            val latLon = LatLng(location.latitude, location.longitude)
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLon, INITIAL_ZOOM_VAL))
            true
        } else false
    }

    private fun askForLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //permission is not granted
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                //show an explanation to the user
                val dialog = PermissionExplanationDialog()
                dialog.show(supportFragmentManager, PERMISSION_EXPLANATION_DIALOG)
            }
            else { //no explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_LOCATION_PERMISSIONS)
            }
        }
        else { //permission has already been granted
            enableMyLocation()
        }
    }

    private fun enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            map.isMyLocationEnabled = true
            map.uiSettings.isMyLocationButtonEnabled = true
            fusedLocationClient.lastLocation.addOnSuccessListener { zoomInOnUserLocation(it) }
            enableRadarAlerts()
        }
    }

    private fun enableRadarAlerts() {
        locationWatcher = LocationWatcher(fusedLocationClient, this)
        locationWatcher?.startWatching()
    }
}
