package ss.virovitica.brzastaza.di

import com.google.android.gms.location.LocationServices
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.module
import org.koin.experimental.builder.singleBy
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ss.virovitica.brzastaza.activity.map.presenter.IMapPresenter
import ss.virovitica.brzastaza.activity.map.presenter.MapPresenter
import ss.virovitica.brzastaza.activity.map.view.IMapView
import ss.virovitica.brzastaza.activity.radar.presenter.IRadarPresenter
import ss.virovitica.brzastaza.activity.radar.presenter.RadarPresenter
import ss.virovitica.brzastaza.activity.radar.view.IRadarView
import ss.virovitica.brzastaza.database.IDatabase
import ss.virovitica.brzastaza.database.RealmDatabase
import ss.virovitica.brzastaza.interactor.geocoding.GeocodeInteractor
import ss.virovitica.brzastaza.interactor.geocoding.IGeocodeInteractor
import ss.virovitica.brzastaza.interactor.radar.IRadarInteractor
import ss.virovitica.brzastaza.interactor.radar.RadarInteractor
import ss.virovitica.brzastaza.net.HakClient
import ss.virovitica.brzastaza.net.GeocodeClient
import ss.virovitica.brzastaza.net.GeocodeClient.Companion.GEOCODE_API_OKHTTP
import ss.virovitica.brzastaza.net.GeocodeClient.Companion.GEOCODE_API_RETROFIT
import ss.virovitica.brzastaza.net.HakClient.Companion.HAK_API_OKHTTP
import ss.virovitica.brzastaza.net.HakClient.Companion.HAK_API_RETROFIT
import ss.virovitica.brzastaza.utility.LocationManager
import java.util.concurrent.TimeUnit

//modules

val generalModule = module {
    singleBy<IDatabase, RealmDatabase>()
    single { RealmDatabase() }
    single { (manager: LocationManager) -> LocationServices.getFusedLocationProviderClient(manager.context) }
}

val netModule = module {
    //HttpLoggingInterceptor
    single {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        interceptor
    }
}

val netModuleHak = module {
    //HakApi
    single<OkHttpClient>(named(HAK_API_OKHTTP)) { //OkHttpClient for HakApi
        OkHttpClient.Builder().addInterceptor(get<HttpLoggingInterceptor>())
            .readTimeout(HakClient.TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .connectTimeout(HakClient.TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .build()
    }
    singleBy<IRadarInteractor, RadarInteractor>()
    single { RadarInteractor(get()) }
    single { provideHakClient(get(named(HAK_API_RETROFIT))) }
    single(named(HAK_API_RETROFIT)) { provideHakRetrofit(get(named(HAK_API_OKHTTP)))}
}
fun provideHakClient(retrofit: Retrofit): HakClient = retrofit.create(HakClient::class.java)
fun provideHakRetrofit(okHttpClient: OkHttpClient): Retrofit =
    Retrofit.Builder()
        .baseUrl(HakClient.BASE_URL)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create()).build() //uses Gson to interpret the response

val netModuleGeocode = module {
    //GeocodeApi
    single<OkHttpClient>(named(GEOCODE_API_OKHTTP)) { //OkHttpClient for GeocodeApi
        OkHttpClient.Builder().addInterceptor(get<HttpLoggingInterceptor>())
            .readTimeout(GeocodeClient.TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .connectTimeout(GeocodeClient.TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .build()
    }
    singleBy<IGeocodeInteractor, GeocodeInteractor>()
    single { GeocodeInteractor(get()) }
    single { provideGeocodeClient(get(named(GEOCODE_API_RETROFIT))) }
    single(named(GEOCODE_API_RETROFIT)) { provideGeocodeRetrofit(get(named(GEOCODE_API_OKHTTP))) }
}
fun provideGeocodeClient(retrofit: Retrofit): GeocodeClient = retrofit.create(GeocodeClient::class.java)
fun provideGeocodeRetrofit(okHttpClient: OkHttpClient): Retrofit =
    Retrofit.Builder()
        .baseUrl(GeocodeClient.BASE_URL)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create()).build() //uses Gson to interpret the response

//activities

val mapModule = module {
    factory<IMapPresenter> { (view: IMapView) -> MapPresenter(view, get(), get(), get()) }
}

val radarModule = module {
    factory<IRadarPresenter> { (view: IRadarView) -> RadarPresenter(view, get(), get()) }
}

//component
val appComponent: List<Module> = listOf(generalModule, netModule, netModuleHak, netModuleGeocode, mapModule, radarModule)
