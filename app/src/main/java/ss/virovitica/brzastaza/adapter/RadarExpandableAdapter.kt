package ss.virovitica.brzastaza.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import kotlinx.android.synthetic.main.view_county_header.view.*
import kotlinx.android.synthetic.main.view_daily_radar.view.*
import ss.virovitica.brzastaza.R
import ss.virovitica.brzastaza.model.CountyRadarData

class RadarExpandableAdapter: BaseExpandableListAdapter() {
    //members
    private var radars: List<CountyRadarData> = ArrayList()

    //functions

    fun setRadars(radarsIn: List<CountyRadarData>) {
        radars = radarsIn //.sortedWith(compareBy {it.radarList.size}).reversed()
        notifyDataSetChanged()
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int) = true
    override fun hasStableIds() = false
    override fun getGroupId(groupPosition: Int): Long = groupPosition.toLong()
    override fun getChildId(groupPosition: Int, childPosition: Int): Long = (groupPosition*childPosition).toLong()
    override fun getGroupCount() = radars.size
    override fun getChildrenCount(groupPosition: Int) = radars[groupPosition].radarList.size
    override fun getGroup(groupPosition: Int) = radars[groupPosition]
    override fun getChild(groupPosition: Int, childPosition: Int) = radars[groupPosition].radarList[childPosition]
    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup): View {
        val inflater = LayoutInflater.from(parent.context)
        val groupView = inflater.inflate(R.layout.view_county_header, parent, false)
        val radarGroup = getGroup(groupPosition)
        groupView.countyText.text = CountyRadarData.INDEX_TO_COUNTY_NAME[radarGroup.countyID-1]
        groupView.radarCountText.text = radarGroup.radarList.size.toString()
        groupView.isClickable = (radarGroup.radarList.size == 0)
        return groupView
    }
    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
        val inflater = LayoutInflater.from(parent.context)
        val childView = inflater.inflate(R.layout.view_daily_radar, parent, false)
        val radar = getChild(groupPosition, childPosition)
        childView.startTimeText.text = radar?.startTime
        childView.endTimeText.text = radar?.endTime
        childView.routeText.text = radar?.route ?: parent.context.getString(R.string.daily_radars_failed)
        return childView
    }
}