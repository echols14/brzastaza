package ss.virovitica.brzastaza.utility

import com.google.android.gms.maps.model.LatLng
import org.junit.Test

class FunctionsTest {
    @Test
    fun getDistanceFromLatLonInKmTest() {
        val zagreb = LatLng(45.785397, 16.074369)
        val osijek = LatLng(45.562359, 18.491936)
        val distance = getDistanceFromLatLonInKm(zagreb.latitude, zagreb.longitude, osijek.latitude, osijek.longitude)
        println(distance)
        assert(distance in 150.0..280.0)
    }
}